
#General Rahen
high_philosophy_school_origin_starry_eye = {
	local_missionary_strength = -0.01
	local_institution_spread = 0.1
	province_trade_power_modifier = 0.05
}
high_philosophy_school_origin_orange_sash = {
	local_missionary_strength = -0.01
	local_institution_spread = 0.1
	province_trade_power_modifier = 0.05
}
high_philosophy_school_origin_golden_palace = {
	local_missionary_strength = -0.01
	local_institution_spread = 0.1
	province_trade_power_modifier = 0.05
}
high_philosophy_school_origin_unbroken_claw = {
	local_missionary_strength = -0.01
	local_institution_spread = 0.1
	province_trade_power_modifier = 0.05
}
high_philosophy_school_origin_radiant_sun = {
	local_missionary_strength = -0.01
	local_institution_spread = 0.1
	province_trade_power_modifier = 0.05
}
high_philosophy_school_origin_silk_turban = {
	local_missionary_strength = -0.01
	local_institution_spread = 0.1
	province_trade_power_modifier = 0.05
}
high_philosophy_school_origin_ascendant_soul = {
	local_missionary_strength = -0.01
	local_institution_spread = 0.1
	province_trade_power_modifier = 0.05
}
high_philosophy_school_origin_true_school = {
	local_missionary_strength = -0.01
	local_institution_spread = 0.1
	province_trade_power_modifier = 0.05
}

#Raj
prabhi_promotion_rejected = {
	liberty_desire = 20
}
prabhi_recently_promoted = {
	liberty_desire = -20
}
senapti_recently_demoted = {
	prestige = -1
}
rajiya_senapti = {
	prestige = 0.5
}
raj_closed_senapti_ranks = {
	legitimacy = 0.5
	same_culture_advisor_cost = -0.15
}
raj_opened_senapti_ranks = {
	free_leader_pool = 1
}

raj_royal_barracks = {
	supply_limit = 2
	local_manpower = 5
	garrison_growth = 0.2
	local_defensiveness = 0.1
}

raj_nahana_bhoja_ongoing = {
	legitimacy = 1
	yearly_absolutism = 1
}
raj_nahana_bhoja_hosted = {
	reduced_liberty_desire = -5
}
raj_nigh_collapse = {
	legitimacy = -1
}

raj_census_full_access_to_ledgers = {
	raj_ministries_loyalty_modifier = 0.1
	raj_ministries_influence_modifier = 0.1
}
raj_census_limited_access_to_ledgers = {
	raj_ministries_loyalty_modifier = -0.1
	raj_ministries_influence_modifier = -0.1
}
raj_neglected_provincial_administration = {
	raj_ministries_loyalty_modifier = -0.05
	vassal_income = -0.05
}

#Mandates
raj_mandate_raja_protection = { #Cannot be attacked
	stability_cost_to_declare_war = 1
}
raj_mandate_right_to_expansion = { #Can expand during high cohesion, lost after declaring war
	ae_impact = -0.1
}
raj_mandate_guaranteed_autonomy = { #Cannot be annexed
	liberty_desire = -10
}
raj_mandate_mercantile_preference = { #Prabhi Only
	global_own_trade_power = 0.2
	raj_ministries_influence_modifier = 0.1
}
raj_mandate_sanctioned_armoury = { #Senapti Only
	land_forcelimit = 2
	discipline = 0.05
	raj_ministries_influence_modifier = 0.1
}
raj_mandate_ministry_lectures = {
	idea_cost = -0.1
	raj_ministries_influence_modifier = 0.1
}
raj_mandate_senapti_primacy = { #Senapti Only, Cannot become Prabhi
	liberty_desire = -10
}
raj_mandate_extended_prabhi_privileges = { #Prabhi Only. Cannot become Senapti
	global_tax_modifier = 0.2
	raj_ministries_influence_modifier = 0.1
}
raj_mandate_territorial_limits = { #Cannot declare war on other members of the Raj, except the Raja
	liberty_desire = 20
}

raj_administration_is_busy = {
}
raj_mandate_sactioned_conquest = {
}
raj_mandate_denied = {
	liberty_desire = 10
}

raj_was_outmaneuvered = {
	diplomatic_reputation = -1
}

#Rahen Court Reforms
rahen_corrupt_ministery_of_agriculture = {
	global_tax_modifier = -0.33
	development_cost = 0.2
	raj_ministries_influence_modifier = 0.05
}
rahen_corrupt_ministery_of_philosophy = {
	advisor_cost = 0.25
	yearly_corruption = 0.5
	raj_ministries_influence_modifier = 0.05
}
rahen_corrupt_ministery_of_internal_relations = {
	reduced_liberty_desire = -15
	vassal_income = -0.15
	raj_ministries_influence_modifier = 0.05
}
rahen_corrupt_ministery_of_foreign_relations = {
	diplomatic_upkeep = -3
	diplomats = -1
	raj_ministries_influence_modifier = 0.05
}

rahen_suspicious_ministry_estate = {
	trade_goods_size_modifier = 0.05
	raj_ministries_influence_modifier = 0.02
}
rahen_neglected_infrastructure = {
	local_build_cost = 0.1
	local_build_time = 0.25
	local_development_cost = 0.1
	local_tax_modifier = -0.1
}
rahen_severely_neglected_infrastructure = {
	local_build_cost = 0.25
	local_build_time = 0.5
	local_development_cost = 0.25
	local_tax_modifier = -0.1
	local_monthly_devastation = 0.1
}

rahen_curtailed_courts = {
	raj_ministries_loyalty_modifier = 0.1
	diplomatic_reputation = 1
	yearly_corruption = -0.1
}

raj_increased_ministries_controls = {
	country_admin_power = -1
}
raj_raja_introspecting = {
	country_diplomatic_power = -1
	stability_cost_to_declare_war = 1
}
raj_raja_aggressive_policies = {
	country_military_power = -1
	province_warscore_cost = -0.15
	spy_offence = 0.33
}

raj_new_tax_policies = {
	global_tax_modifier = 0.10
	vassal_income = 0.2
}
raj_raja_delegation = {
	development_cost = -0.10
	global_prosperity_growth = 0.5
}
raj_raja_neglected = {
	development_cost = 0.10
	global_tax_modifier = -0.10
}

raj_angered_elephant_riders = {
	cavalry_power = -0.10
}
raj_trip_to_tianlou = {
	idea_cost = -0.10
	tolerance_own = 1
}
raj_refused_vizier_school = {
	idea_cost = 0.10
}

raj_demanded_introspection = { }
raj_ai_introspecting = { }
raj_tutoring_heir = { }
raj_cracking_on_decadence = { }
raj_regulating_vizier = { }
raj_ministerial_census = { }

#Blood Lotus Rebellion
blood_lotus_guerrillas = {
	local_monthly_devastation = 10
	local_tax_modifier = -0.5
	local_development_cost = 0.5
	trade_goods_size = 0.01
	picture = "blood_lotus_guerrillas"
}
blood_lotus_guerrillas_hidden = {
	
}
blood_lotus_sympathizers = {
	local_autonomy = 0.5
	trade_goods_size = 0.01
	local_defensiveness = -0.25
	local_unrest = 5
	picture = "blood_lotus_sympathizers"
}
blood_lotus_headquarter = {
	local_defensiveness = 0.2
	fort_level = 1
	local_autonomy = 1
	local_tax_modifier = -1
	local_development_cost = 1
	trade_goods_size = 0.01
	picture = "blood_lotus_headquarter"
}
blood_lotus_rebel_negotiation = {
	global_unrest = -2
}
blood_lotus_hardline_stance = {
	global_unrest = 5
}
blood_lotus_subdued = {
	global_unrest = -2
	stability_cost_modifier = -0.1
}
blood_lotus_powerful_government = {
	harsh_treatment_cost = -0.5
	global_autonomy = -0.2
	global_tax_modifier = 0.2
	production_efficiency = 0.2
	yearly_absolutism = 1
}
blood_lotus_dissuaded_sympathizers = {
	local_unrest = -5
}

#Hobgoblins
hobgoblin_bloodsong_advance_guard = {
	shock_damage = 0.1
	shock_damage_received = -0.05
}
hobgoblin_thunderfist_shamans = {
	fire_damage = 0.05
	fire_damage_received = -0.05
}
hobgoblin_riches_of_jade_mines = {
	global_tax_income = 6
}
command_only_internal_trade = {
	trade_efficiency = -0.25
	global_trade_power = -0.5
	global_prov_trade_power_modifier = 0.33
	embargo_efficiency = 0.5
}
command_rule_of_the_wolf = {
	hob_wolf_command_influence = 0.1
	wolf_command_loyalty_modifier = 0.1
}
command_rule_of_the_boar = {
	hob_boar_command_influence = 0.1
	boar_command_loyalty_modifier = 0.1
}
command_rule_of_the_lion = {
	hob_lion_command_influence = 0.25
	lion_command_loyalty_modifier = 0.1
}
command_rule_of_the_dragon = {
	hob_wolf_command_influence = 0.1
	dragon_command_loyalty_modifier = 0.1
}
command_rule_of_the_elephant = {
	hob_boar_command_influence = 0.1
	elephant_command_loyalty_modifier = 0.1
}
command_rule_of_the_tiger = {
	hob_lion_command_influence = 0.1
	tiger_command_loyalty_modifier = 0.1
}
command_unproven_leadership = {
	all_estate_loyalty_equilibrium = -0.1
	land_morale = -0.05
	legitimacy = -2
}
command_exemplary_leadership = {
	all_estate_loyalty_equilibrium = 0.05
	land_morale = 0.05
	legitimacy = 1
}

command_oni_siege_magic = { #This really needs to change names
	siege_ability = 0.15
}
command_orc_slaves = {
	trade_goods_size = 0.5
}

command_on_the_march_1 = {
	movement_speed = 0.05
	core_creation = -0.05
	province_warscore_cost = -0.05
}
command_on_the_march_2 = {
	movement_speed = 0.10
	core_creation = -0.10
	province_warscore_cost = -0.10
}
command_on_the_march_3 = {
	movement_speed = 0.15
	core_creation = -0.15
	province_warscore_cost = -0.15
}
command_swords_and_dumplings = {
	manpower_recovery_speed = 0.1
}
command_secure_supply_lines = {
	land_attrition = -0.1
	reinforce_speed = 0.1
	recover_army_morale_speed = 0.1
}
command_support_of_the_marshals = {
	land_morale = 0.1
}
command_preparing_homesteads = {
	build_time = -0.5
	development_cost = 1 
}
command_secure_korashi_supply = {
	shock_damage_received = -0.05
}
command_stockpiling_korashi_chains = {#! no idea what this does, but it appears in the MT as something you don't want for a mission
	
}
command_mithril_weaponry = {
	shock_damage = 0.1
	shock_damage_received = -0.05
}
command_reacclimatising_to_the_surface = {
	development_cost = 0.2
	build_cost = 0.2
	global_unrest  = 1
}
command_shifting_standards = {
	idea_cost = -0.1
	global_unrest  = 1
	general_cost = -0.1
	gerunanin_loyalty_modifier = 0.1
}
command_a_new_age_of_warfare = {
	backrow_artillery_damage = 0.1
	siege_ability = 0.1
}
command_shinobi = {
	spy_offence = 0.2
}

command_wolf_warcamp = {
	
}
command_boar_warcamp = {
	
}
command_lion_warcamp = {
	
}
command_tiger_warcamp = {
	
}
command_elephant_warcamp = {
	
}
command_dragon_warcamp = {
	
}
command_expanded_estates = {
	local_development_cost = -0.1
}
command_sarilavhan_foundries = {
	trade_goods_size = 1.5
	production_efficiency = 0.15
	global_manpower = 5
}
command_the_mithril_forge = {
	shock_damage = 0.1
	shock_damage_received = -0.05
	trade_value_modifier = 0.25
}
