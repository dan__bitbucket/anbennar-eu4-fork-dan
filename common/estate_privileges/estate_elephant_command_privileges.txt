
estate_elephant_command_land_rights = {
	icon = privilege_grant_autonomy
	loyalty = 0.05
	influence = 0.05
	land_share = 5
	max_absolutism = -5
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		governing_capacity = 100
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_elephant_command_x = {
	#icon = 
	loyalty = 0.1
	influence = 0.1
	land_share = 0
	max_absolutism = -10
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_elephant_command_construction_subsidies = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		build_cost = -0.10
		build_time = -0.10
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_elephant_command_fortification_experts = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		defensiveness = 0.10
	}
	
	conditional_modifier = {
		trigger = { faction_in_power = hob_elephant_command }
		modifier = {
			fort_maintenance_modifier = -0.20
		}
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_elephant_command_management = {	# this need more details
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
		if = {
			limit = {
				any_owned_province = {
					has_province_modifier = hob_elephant_war_camp # make this check teh correct province/modifier
				}
			}
			custom_tooltip = "Add a cool modifier in the Elephant War Camp"
		}
		else = {
			custom_tooltip = "Has no effects upon being granted."
		}
	}
	
	on_granted_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_elephant_war_camp } # make this check teh correct province/modifier
			custom_tooltip = estate_elephant_command_management_tt
			hidden_effect = { add_province_triggered_modifier = hob_elephant_management }
		}
	}
	
	on_revoked = {
		custom_tooltip = "Remove the cool modifier in the Elephant War Camp"
	}
	
	on_revoked_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_elephant_management }
			custom_tooltip = revoke_estate_elephant_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_elephant_management }
		}
	}
	
	on_invalid = {
		custom_tooltip = "Remove the cool modifier in the Elephant War Camp"
	}
	
	on_invalid_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_elephant_management }
			custom_tooltip = revoke_estate_elephant_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_elephant_management }
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	modifier_by_land_ownership = {
		build_cost = -0.25
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}
