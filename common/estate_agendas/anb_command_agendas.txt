

#Standard Agendas
estate_boar_command_hire_advisor = {
	can_select = {
		NOT = {
			employed_advisor = {
				category = MIL
			}
		}
	}
	selection_weight = {
		factor = 1
	}
	task_requirements = {
		employed_advisor = {
			category = MIL
		}
		is_in_deficit = no
	}
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_boar_command
			loyalty = 10
		}
    }
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_boar_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}
estate_boar_command_fire_advisor = {
	can_select = {
		employed_advisor = {
			category = MIL
		}
	}
	selection_weight = {
		factor = 1
		modifier = {
			factor = 0.5
			always = yes
		}
	}
	immediate_effect = {
		hidden_effect = {
			set_country_flag = need_to_fire_mil_advisor
		}
	}
	on_invalid = {
		clr_country_flag = need_to_fire_mil_advisor
	}
	task_requirements = {
		custom_trigger_tooltip = {
			tooltip = fire_mil_advisor_tooltip
			has_country_flag = just_fired_mil_advisor
		}
	}
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_boar_command
			loyalty = 10
		}
		clr_country_flag = just_fired_mil_advisor
		clr_country_flag = need_to_fire_mil_advisor
	}
	invalid_trigger = {
		NOT = {
			employed_advisor = {
				category = MIL
			}
		}
		NOT = { has_country_flag = just_fired_mil_advisor }
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_boar_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
		clr_country_flag = need_to_fire_mil_advisor
    }
}

estate_lion_command_hire_advisor = {
	can_select = {
		NOT = {
			employed_advisor = {
				category = MIL
			}
		}
	}
	selection_weight = {
		factor = 1
	}
	task_requirements = {
		employed_advisor = {
			category = MIL
		}
		is_in_deficit = no
	}
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_lion_command
			loyalty = 10
		}
    }
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_lion_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}
estate_lion_command_fire_advisor = {
	can_select = {
		employed_advisor = {
			category = MIL
		}
	}
	selection_weight = {
		factor = 1
		modifier = {
			factor = 0.5
			always = yes
		}
	}
	immediate_effect = {
		hidden_effect = {
			set_country_flag = need_to_fire_mil_advisor
		}
	}
	on_invalid = {
		clr_country_flag = need_to_fire_mil_advisor
	}
	task_requirements = {
		custom_trigger_tooltip = {
			tooltip = fire_mil_advisor_tooltip
			has_country_flag = just_fired_mil_advisor
		}
	}
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_lion_command
			loyalty = 10
		}
		clr_country_flag = just_fired_mil_advisor
		clr_country_flag = need_to_fire_mil_advisor
	}
	invalid_trigger = {
		NOT = {
			employed_advisor = {
				category = MIL
			}
		}
		NOT = { has_country_flag = just_fired_mil_advisor }
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_lion_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
		clr_country_flag = need_to_fire_mil_advisor
    }
}

estate_wolf_command_hire_advisor = {
	can_select = {
		NOT = {
			employed_advisor = {
				category = ADM
			}
		}
	}
	selection_weight = {
		factor = 1
	}
	task_requirements = {
		employed_advisor = {
			category = ADM
		}
		is_in_deficit = no
	}
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_wolf_command
			loyalty = 10
		}
    }
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_wolf_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}
estate_wolf_command_fire_advisor = {
	can_select = {
		employed_advisor = {
			category = ADM
		}
	}
	selection_weight = {
		factor = 1
		modifier = {
			factor = 0.5
			always = yes
		}
	}
	immediate_effect = {
		hidden_effect = {
			set_country_flag = need_to_fire_mil_advisor
		}
	}
	on_invalid = {
		clr_country_flag = need_to_fire_mil_advisor
	}
	task_requirements = {
		custom_trigger_tooltip = {
			tooltip = fire_mil_advisor_tooltip
			has_country_flag = just_fired_mil_advisor
		}
	}
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_wolf_command
			loyalty = 10
		}
		clr_country_flag = just_fired_mil_advisor
		clr_country_flag = need_to_fire_mil_advisor
	}
	invalid_trigger = {
		NOT = {
			employed_advisor = {
				category = ADM
			}
		}
		NOT = { has_country_flag = just_fired_adm_advisor }
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_wolf_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
		clr_country_flag = need_to_fire_mil_advisor
    }
}


#Retake Core
estate_lion_command_retake_core = {
	can_select = {
		is_free_or_tributary_trigger = yes
		OR = {
			is_part_of_hre = no
			NOT = { hre_reform_passed = landfriede }
		}
		any_province = {
			is_core = root
			NOT = { country_or_non_sovereign_subject_holds = root }
			is_city = yes
			NOT = { owner = { ROOT = { truce_with = prev } } }
		}
	}
	selection_weight = {
		factor = 2
		modifier = {
			factor = 0.5
			any_province = {
				is_core = root
				NOT = { country_or_non_sovereign_subject_holds = root }
				is_city = yes
				owner = {
					NOT = { ROOT = { truce_with = prev } }
					alliance_with = root
				}
			}
		}
		modifier = {
			factor = 0.25
			any_country = {
				coalition_target = root
			}
		}
	}
	pre_effect = {
		random_province = {
			limit = {
				is_core = root
				NOT = { country_or_non_sovereign_subject_holds = root }
				is_city = yes
				NOT = { owner = { ROOT = { truce_with = prev } } }
			}
			save_event_target_as = agenda_province
		}
		random_province = {
			limit = {
				is_core = root
				NOT = { country_or_non_sovereign_subject_holds = root }
				is_city = yes
				NOT = { owner = { ROOT = { truce_with = prev } } }
				NOT = { owner = { alliance_with = root } }
			}
			save_event_target_as = agenda_province
		}
		random_province = {
			limit = {
				is_core = root
				NOT = { country_or_non_sovereign_subject_holds = root }
				is_city = yes
				NOT = { owner = { ROOT = { truce_with = prev } } }
				OR = {
					AND = {
						has_port = yes
						root = {
							any_owned_province = {
								has_port = yes
							}
						}
					}
					any_neighbor_province = {
						owned_by = root
					}
				}
			}
			save_event_target_as = agenda_province
		}
		random_province = {
			limit = {
				is_core = root
				NOT = { country_or_non_sovereign_subject_holds = root }
				is_city = yes
				NOT = { owner = { ROOT = { truce_with = prev } } }
				OR = {
					AND = {
						has_port = yes
						root = {
							any_owned_province = {
								has_port = yes
							}
						}
					}
					any_neighbor_province = {
						owned_by = root
					}
				}
				NOT = { owner = { alliance_with = root } }
			}
			save_event_target_as = agenda_province
		}
	}
	fail_if = {
		event_target:agenda_province = {
			NOT = { is_core = root }
		}
	}
	provinces_to_highlight = {
		province_id = event_target:agenda_province
	}
	task_requirements = {
		event_target:agenda_province = {
			owned_by = root
		}
	}
	task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_lion_command
			loyalty = 10
		}
		add_country_modifier = {
			name = military_victory
			duration = 3650
		}
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_lion_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}

#Crush Revolts (not heretics)
estate_boar_command_crush_revolts = {
	can_select = {
		num_of_rebel_armies = 1
		NOT = {
			OR = {
				has_spawned_rebels = noble_rebels
				has_spawned_rebels = polish_noble_rebels
				#Clergy agenda covers heretic rebels
				has_spawned_rebels = heretic_rebels
				has_spawned_rebels = lollard_rebels
			}
		}
	}
	selection_weight = {
		factor = 2
		modifier = {
			factor = 2
			num_of_rebel_controlled_provinces = 3
		}
		modifier = {
			factor = 2
			num_of_rebel_controlled_provinces = 5
		}
	}
	provinces_to_highlight = {
		owned_by = root
		controlled_by = REB
	}
	fail_if = {
		has_country_modifier = just_lost_to_rebels
	}
	task_requirements = {
		NOT = { num_of_rebel_controlled_provinces = 1 }
		NOT = { num_of_rebel_armies = 1 }
		hidden_trigger = { NOT = { has_country_modifier = just_lost_to_rebels } }
	}
	task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_boar_command
			loyalty = 10
		}
		add_prestige = 10
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_boar_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}


#Conquer X area from country you don't have great relations with
estate_wolf_command_expand_into_x = {
	can_select = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		any_neighbor_country = {
			num_of_cities = 6
			is_free_or_tributary_trigger = yes
			NOT = { is_subject_of = root }
			NOT = { overlord_of = root }
			NOT = { alliance_with = root }
			NOT = { ROOT = { truce_with = prev } }
			NOT = { 
				has_opinion = {
					who = root
					value = 25
				}
			}
			NOT = {
				any_owned_province = { #No need for this agenda if covered by a mission
					OR = {
						is_permanent_claim = root
						is_core = root
					}
				}
			}
			ROOT = {
				knows_country = prev
			}
			any_owned_province = {
				any_neighbor_province = {
					owned_by = root
				}
				NOT = {
					area_for_scope_province = {
						OR = {
							is_empty = yes
							owned_by = root
						}
					}
				}
			}
			OR = {
				is_part_of_hre = no
				NOT = { hre_reform_passed = landfriede }
				ROOT = { is_part_of_hre = no }
			}
		}
	}
	selection_weight = {
		factor = 2
		modifier = {
			factor = 0.5
			has_non_generic_missions = yes
		}
		modifier = {
			factor = 1.5
			num_of_cities = 20
		}
		modifier = {
			factor = 1.5
			num_of_cities = 25
		}
		modifier = {
			factor = 1.5
			num_of_cities = 30
		}
		modifier = {
			factor = 1.5
			num_of_cities = 40
		}
		modifier = {
			factor = 1.5
			num_of_cities = 50
		}
		modifier = {
			factor = 0.25
			any_country = {
				coalition_target = root
			}
		}
	}
	pre_effect = {
		random_neighbor_country = {
			limit = {
				num_of_cities = 6
				is_free_or_tributary_trigger = yes
				NOT = { is_subject_of = root }
				NOT = { overlord_of = root }
				NOT = { alliance_with = root }
				NOT = { ROOT = { truce_with = prev } }
				NOT = { 
					has_opinion = {
						who = root
						value = 25
					}
				}
				NOT = {
					any_owned_province = { #No need for this agenda if covered by a mission
						OR = {
							is_permanent_claim = root
							is_core = root
						}
					}
				}
				ROOT = {
					knows_country = prev
				}
				any_owned_province = {
					any_neighbor_province = {
						owned_by = root
					}
					NOT = {
						area_for_scope_province = {
							owned_by = root
						}
					}
				}
				OR = {
					is_part_of_hre = no
					NOT = { hre_reform_passed = landfriede }
					ROOT = { is_part_of_hre = no }
				}
			}
			random_owned_province = {
				limit = {
					any_neighbor_province = {
						owned_by = root
					}
					area_for_scope_province = {
						type = all
						NOT = { owned_by = root }
						is_empty = no
					}
				}
				save_event_target_as = agenda_province
			}
		}
	}
	immediate_effect = {
		if = {
			limit = {
				event_target:agenda_province = {
					area_for_scope_province = {
						NOT = { is_claim = root }
						NOT = { is_core = root }
					}
				}
			}
			event_target:agenda_province = {
				area = {
					limit = {
						NOT = { is_claim = root }
						NOT = { is_core = root }
					}
					add_claim = root
				}
			}
		}
	}
	on_invalid = {
		if = {
			limit = {
				event_target:agenda_province = {
					area_for_scope_province = {
						is_claim = root
						NOT = { is_permanent_claim = root }
					}
				}
			}
			event_target:agenda_province = {
				area = {
					limit = {
						is_claim = root
						NOT = { is_permanent_claim = root }
					}
					remove_claim = root
				}
			}
		}
	}
	provinces_to_highlight = {
		area_for_scope_province = {
			province_id = event_target:agenda_province
		}
	}
	task_requirements = {
		event_target:agenda_province = {
			area_for_scope_province = {
				owned_by = root
			}
		}
	}
	fail_if = {
		event_target:agenda_province = {
			area_for_scope_province = {
				type = all
				NOT = { is_claim = root }
				NOT = { is_core = root }
				NOT = { owned_by = root }
			}
		}
	}
	task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_wolf_command
			loyalty = 15
		}
		add_adm_power = 50
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_wolf_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
		if = {
			limit = {
				event_target:agenda_province = {
					area_for_scope_province = {
						is_claim = root
						NOT = { is_permanent_claim = root }
					}
				}
			}
			event_target:agenda_province = {
				area = {
					limit = {
						is_claim = root
						NOT = { is_permanent_claim = root }
					}
					remove_claim = root
				}
			}
		}
    }
}

#Complete conquest of X area from country you don't have great relations with
estate_wolf_command_complete_conquest_of_x = {
	can_select = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		any_neighbor_country = {
			num_of_cities = 6
			is_free_or_tributary_trigger = yes
			NOT = { is_subject_of = root }
			NOT = { overlord_of = root }
			NOT = { alliance_with = root }
			NOT = { ROOT = { truce_with = prev } }
			NOT = { 
				has_opinion = {
					who = root
					value = 25
				}
			}
			NOT = {
				any_owned_province = { #No need for this agenda if covered by a mission
					OR = {
						is_permanent_claim = root
						is_core = root
					}
				}
			}
			ROOT = {
				knows_country = prev
			}
			any_owned_province = {
				any_neighbor_province = {
					owned_by = root
				}
				area_for_scope_province = {
					owned_by = root
				}
				NOT = {
					area_for_scope_province = {
						owner = {
							OR = {
								alliance_with = root
								ROOT = { truce_with = prev }
								is_subject_of = root
								AND = {
									is_part_of_hre = yes
									hre_reform_passed = landfriede
								}
							}
						}
					}
				}
			}
		}
	}
	selection_weight = {
		factor = 2.5
		modifier = {
			factor = 0.5
			has_non_generic_missions = yes
		}
		modifier = {
			factor = 1.5
			num_of_cities = 20
		}
		modifier = {
			factor = 1.5
			num_of_cities = 25
		}
		modifier = {
			factor = 1.5
			num_of_cities = 30
		}
		modifier = {
			factor = 1.5
			num_of_cities = 40
		}
		modifier = {
			factor = 1.5
			num_of_cities = 50
		}
		modifier = {
			factor = 0.25
			any_country = {
				coalition_target = root
			}
		}
	}
	pre_effect = {
		random_neighbor_country = {
			limit = {
				num_of_cities = 6
				is_free_or_tributary_trigger = yes
				NOT = { is_subject_of = root }
				NOT = { overlord_of = root }
				NOT = { alliance_with = root }
				NOT = { ROOT = { truce_with = prev } }
				NOT = { 
					has_opinion = {
						who = root
						value = 25
					}
				}
				NOT = {
					any_owned_province = { #No need for this agenda if covered by a mission
						OR = {
							is_permanent_claim = root
							is_core = root
						}
					}
				}
				ROOT = {
					knows_country = prev
				}
				any_owned_province = {
					any_neighbor_province = {
						owned_by = root
					}
					area_for_scope_province = {
						owned_by = root
					}
					NOT = {
						area_for_scope_province = {
							owner = {
								OR = {
									alliance_with = root
									ROOT = { truce_with = prev }
									is_subject_of = root
									AND = {
										is_part_of_hre = yes
										hre_reform_passed = landfriede
									}
								}
							}
						}
					}
				}
			}
			random_owned_province = {
				limit = {
					any_neighbor_province = {
						owned_by = root
					}
					area_for_scope_province = {
						owned_by = root
					}
					NOT = {
						area_for_scope_province = {
							owner = {
								OR = {
									alliance_with = root
									ROOT = { truce_with = prev }
									is_subject_of = root
									AND = {
										is_part_of_hre = yes
										hre_reform_passed = landfriede
									}
								}
							}
						}
					}
				}
				save_event_target_as = agenda_province
			}
		}
	}
	immediate_effect = {
		if = {
			limit = {
				event_target:agenda_province = {
					area_for_scope_province = {
						NOT = { is_claim = root }
						NOT = { is_core = root }
					}
				}
			}
			event_target:agenda_province = {
				area = {
					limit = {
						NOT = { is_claim = root }
						NOT = { is_core = root }
					}
					add_claim = root
				}
			}
		}
	}
	on_invalid = {
		if = {
			limit = {
				event_target:agenda_province = {
					area_for_scope_province = {
						is_claim = root
						NOT = { is_permanent_claim = root }
					}
				}
			}
			event_target:agenda_province = {
				area = {
					limit = {
						is_claim = root
						NOT = { is_permanent_claim = root }
					}
					remove_claim = root
				}
			}
		}
	}
	provinces_to_highlight = {
		area_for_scope_province = {
			province_id = event_target:agenda_province
		}
		NOT = { owned_by = root }
	}
	task_requirements = {
		event_target:agenda_province = {
			area_for_scope_province = {
				type = all
				owned_by = root
			}
		}
	}
	fail_if = {
		event_target:agenda_province = {
			area_for_scope_province = {
				type = all
				NOT = { is_claim = root }
				NOT = { is_core = root }
				NOT = { owned_by = root }
			}
		}
	}
	task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_wolf_command
			loyalty = 15
		}
		add_adm_power = 50
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_wolf_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
		if = {
			limit = {
				event_target:agenda_province = {
					area_for_scope_province = {
						is_claim = root
						NOT = { is_permanent_claim = root }
					}
				}
			}
			event_target:agenda_province = {
				area = {
					limit = {
						is_claim = root
						NOT = { is_permanent_claim = root }
					}
					remove_claim = root
				}
			}
		}
    }
}

#Recover bad prestige
estate_lion_command_recover_abysmal_prestige = {
	can_select = {
		NOT = { prestige = -30 }
	}
	selection_weight = {
		factor = 8
	}
	pre_effect = {
		set_variable = {
			which = estate_nobles_improve_prestige_var
			value = 0
		}
		if = {
			limit = {
				NOT = {
					prestige = -75
				}
			}
			set_variable = {
				which = estate_nobles_improve_prestige_var
				value = 1
			}
		}
		else = {
			set_variable = {
				which = estate_nobles_improve_prestige_var
				value = 2
			}
		}
	}
	task_requirements = {
		if = {
			limit = {
				check_variable = {
					which = estate_nobles_improve_prestige_var
					value = 2
				}
			}
			prestige = 0
		}
		else = {
			prestige = -30
		}
	}
	task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 10
		}
		add_stability_or_adm_power = yes
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_nobles
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}


#Get good prestige
estate_lion_command_improve_prestige = {
	can_select = {
		prestige = -30
		NOT = { prestige = 20 }
	}
	selection_weight = {
		factor = 2.5
		modifier = {
			factor = 2
			NOT = {
				prestige = 0
			}
		}
	}
	pre_effect = {
		set_variable = {
			which = estate_nobles_improve_prestige_var
			value = 0
		}
		if = {
			limit = {
				NOT = {
					prestige = -10
				}
			}
			set_variable = {
				which = estate_nobles_improve_prestige_var
				value = 1
			}
		}
		else_if = {
			limit = {
				NOT = {
					prestige = 15
				}
			}
			set_variable = {
				which = estate_nobles_improve_prestige_var
				value = 2
			}
		}
		else = {
			set_variable = {
				which = estate_nobles_improve_prestige_var
				value = 3
			}
		}
	}
	task_requirements = {
		if = {
			limit = {
				check_variable = {
					which = estate_nobles_improve_prestige_var
					value = 3
				}
			}
			prestige = 50
		}
		else_if = {
			limit = {
				check_variable = {
					which = estate_nobles_improve_prestige_var
					value = 2
				}
			}
			prestige = 30
		}
		else = {
			prestige = 10
		}
	}
	task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 10
		}
		add_adm_power = 50
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_nobles
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}


#Bigger Army
estate_boar_command_build_an_army = {
	can_select = {
		NOT = { has_country_modifier = thriving_arms_industry }
		NOT = { army_size_percentage = 0.5 }
		if = {
			limit = {
				is_emperor = yes
			}
			NOT = { army_size_percentage = 0.35 }
		}
	}
	selection_weight = {
		factor = 5
		modifier = {
			factor = 2
			any_rival_country = {
				NOT = { truce_with = root }
				army_size = root
			}
		}
		modifier = {
			factor = 2.0
			NOT = { army_size_percentage = 0.3 }
			NOT = { army_size = 150 }
		}
		modifier = { #this could be really annoying in the late game
			factor = 0.5
			army_size = 100
		}
		modifier = {
			factor = 0.25
			army_size = 200
		}
		modifier = {
			factor = 0
			army_size = 500
		}
		modifier = {
			factor = 0.5
			is_emperor = yes
		}
	}
	task_requirements = {
		if = {
			limit = {
				is_emperor = yes
			}
			army_size_percentage = 0.5
		}
		else = {
			army_size_percentage = 0.75
		}
	}
	task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_boar_command
			loyalty = 10
		}
		create_general = {
			tradition = 40
		}
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_boar_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}


#Bigger Army than Rival
estate_boar_command_bigger_army_than_rival = {
	can_select = {
		NOT = { has_country_modifier = thriving_arms_industry }
		NOT = { army_size_percentage = 1 }
		any_rival_country = {
			NOT = { war_with = root }
			NOT = { root = { army_size = prev } }
			army_balance = {
				who = root
				value = 1.05
			}
			NOT = {
				army_balance = {
					who = root
					value = 1.25
				}
			}
		}
	}
	selection_weight = {
		factor = 5
	}
	pre_effect = {
		random_rival_country = {
			limit = {
				NOT = { war_with = root }
				NOT = { root = { army_size = prev } }
				army_balance = {
					who = root
					value = 1.05
				}
				NOT = {
					army_balance = {
						who = root
						value = 1.25
					}
				}
			}
			save_event_target_as = agenda_country
		}
	}
	provinces_to_highlight = {
		is_capital_of = event_target:agenda_country
	}
	task_requirements = {
		if = {
			limit = { event_target:agenda_country = { num_of_cities = 1 } }
			army_size = event_target:agenda_country
		}
		else = {
			always = yes
		}
	}
	task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_boar_command
			loyalty = 10
		}
		add_country_modifier = {
			name = thriving_arms_industry
			duration = 3650
		}
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_boar_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}

#Manpower reserves
estate_wolf_command_build_up_manpower_reserves = {
	can_select = {
		is_at_war = no
		NOT = { manpower_percentage = 0.40 }
	}
	selection_weight = {
		factor = 5
		modifier = {
			factor = 1.5
			NOT = { manpower_percentage = 0.35 }
		}
		modifier = {
			factor = 1.5
			NOT = { manpower_percentage = 0.3 }
		}
		modifier = {
			factor = 1.5
			NOT = { manpower_percentage = 0.25 }
		}
		modifier = {
			factor = 2
			NOT = { manpower_percentage = 0.2 }
		}
		modifier = {
			factor = 3
			NOT = { manpower_percentage = 0.15 }
		}
	}
	pre_effect = {
		set_variable = {
			which = estate_wolf_command_build_up_manpower_reserves_variable
			value = 0
		}
		if = {
			limit = {
				NOT = {
					manpower_percentage = 0.1
				}
			}
			set_variable = {
				which = estate_wolf_command_build_up_manpower_reserves_variable
				value = 1
			}
		}
		else_if = {
			limit = {
				NOT = {
					manpower_percentage = 0.3
				}
			}
			set_variable = {
				which = estate_wolf_command_build_up_manpower_reserves_variable
				value = 2
			}
		}
		else = {
			set_variable = {
				which = estate_wolf_command_build_up_manpower_reserves_variable
				value = 3
			}
		}
		pick_type_of_military_advisor = yes
	}
	fail_if = {
		is_at_war = yes
	}
	task_requirements = {
		if = {
			limit = {
				check_variable = {
					which = estate_wolf_command_build_up_manpower_reserves_variable
					value = 3
				}
			}
			manpower_percentage = 0.9
		}
		else_if = {
			limit = {
				check_variable = {
					which = estate_wolf_command_build_up_manpower_reserves_variable
					value = 2
				}
			}
			manpower_percentage = 0.75
		}
		else = {
			manpower_percentage = 0.5
		}
	}
	task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_wolf_command
			loyalty = 10
		}
		generate_scaled_military_advisor_of_religion_effect = {
			religion = root
			discount = yes
		}
		clear_random_military_advisor_generation_flags = yes
	}
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_wolf_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
		clear_random_military_advisor_generation_flags = yes
    }
}



#################
### Campaigns ###
#################

estate_wolf_command_campaign_shamakhad = {
	can_select = {
		always = yes
	}
	selection_weight = {
		factor = 1
	}
	immediate_effect = {
		set_country_flag = command_shamakhad_campaign_activated_flag
		swap_non_generic_missions = yes
		
		#add permanent claims to target provinces and give the appropriate cb
	}
	task_requirements = {
		mission_completed = R62_start_the_shamakhad_campaign
	}
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_wolf_command
			loyalty = 10
		}
    }
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_wolf_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}

estate_boar_command_campaign_xiadao = {
	can_select = {
		always = yes
	}
	selection_weight = {
		factor = 1
	}
	immediate_effect = {
		set_country_flag = command_xiadao_campaign_activated_flag
		swap_non_generic_missions = yes
		
		#add permanent claims to target provinces and give the appropriate cb
	}
	task_requirements = {
		mission_completed = R62_start_the_xiadao_campaign
	}
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_boar_command
			loyalty = 10
		}
    }
    failing_effect = {
        add_estate_loyalty_modifier = {
            estate = estate_boar_command
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}



